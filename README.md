# treinamento-git

## O que é Git?
  - VCS
     - Sistema de versionamento de arquivo ao longo do tempo e permite que você busque por versões especificas e ainda realziar o compartilhamento com outras pessoas.

  - Tipos de VCS
    - Local: Gera copias/versões no disco local da maquina
      - Ferramenta: RCS
    - Centralizado: Mais popular ao longo dos anos. Possui um unico servidor centralizado com todos os arquivos e os clientes pedem esses arquivos a esse servidor
      - Ferramenta: Subversion
    
    - DVCS: VCS distribuido. Todo cliente possui a copia completa do repositorio. Caso de algum problema no servidor, é possivel restaurar o repositorio.


## Git

- Historia kernel Linux

  - Entre 1991 e 2002 periodo de desenvolvimento do kernel do linux as mudanças eram feitas a partir de paths e passadas atraves de arquivos compactados
  - 2002 a equipe de desenvolvimento do kernel do linux incluido linus torvalds decidiram usar um DVCS proprietario chamado bitKeeper
  - 2005 o relacionamento entre a empresa e a comunidade começou a ficar estranha
  - 2005 a equipe de desenvolvimento do kernel linux decidiram criar uma propria ferramenta.
  - então surgiu o git FREE :D

- Informações

  - Quase todas as operações são locais
  - É realizado as alterações e depois é feito o "push" para o servidor/repositorio
  - Git não é Github/Gitlab/Bitbucket
  - Github/Gitlab/Bitbucket são servidores/repositorios onde é possivel hospedar os códigos e utilizar a ferramenta git para controlar a versão
  - Git `!=` Github/Gitlab/Bitbucket


## Live Code
  - `$ git init`
    - Cria um repositorio git local
  - `$ git status`
    - Mostra status do seu repositorio atual
  - `$ git add`
    - Adiona arquivos para index de stage
  - `$ git commit`
    - Salva mudanças no repositorio atual local
  - `$ git pull`
    - Atualiza repositorio local a partir de servidor/repositorio ou outra branch
  - `$ git push`
    - Atualiza repositorio/servidor
  - `$ git rebase`
    - *******
  - `$ git diff`
    - Mostra diferença entre repositorio atual e estado do ultimo commit
  - `$ git merge`
    - Junta duas ou mais branches em uma
  - `$ git checkout`
    - Muda de branches ou da um restore repositorio atual
  - `$ git stash`
    - tira o estado sujo do seu diretório de trabalho — isto é, seus arquivos modificados que estão sendo rastreados e mudanças na área de seleção — e o salva em uma pilha de modificações inacabadas que você pode voltar a qualquer momento.
  - `$ git clone`
    - Clona repositorio em um diretorio
  - `$ git branch`
    - Lista, cria ou deleta branchs
  - `$ git log`
    - Mostra historico de commits
  - `$ git reset`
    - Reseta repositorio atual para estado especifico
  - `$ git cherry pick`
    - Aplica mudanças a partir de commits existentes
  - `.gitignore`
    - Arquivo do git para ignorar arquivos a serem commitados
